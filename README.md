![](https://fidelizii.com.br/wp/wp-content/themes/fidelizi-v3/assets/img/fidelizi-logo.png)
# Teste FideliZi! #

A idéia do nosso teste é analisar o conhecimento técnico do desenvolvedor, propondo um teste com tecnologias que ele irá trabalhar diariamente
dentro do FideliZi. 

Algo que prezamos bastante são pessoas que estão sempre buscando conhecimento e que sejam solucionadoras de problemas.


O objetivo aqui é construir uma simples API utilizando Lumen

Lumen Framework [https://lumen.laravel.com/](https://lumen.laravel.com/)  

Faça uma instalação limpa do Lumen utilizando o  composer create-project  

```
composer create-project --prefer-dist laravel/lumen {folder-name}
```

Estruture a seguinte organização para a API:

- `POST /api/v2/login` [Efetuar Login]  
```javascript
// Request
{
	"email": "",
	"senha": ""
}

// Response
{
	"token": "xxxxxxxxxxxxxx"
}

```
- `GET /api/v2/estabelecimentos/{id}` [Retornar informações do estabelecimento]
```javascript
// Response
{
	"id": "",
	"nome": "",
}

```
- `GET /api/v2/estabelecimentos/{id}/clientes` [Retornar todos os clientes de um estabelecimento]
```javascript
// Response
[
    {
        "id": "",
        "nome": "",
        "email": ""
    },
    {
        "id": "",
        "nome": "",
        "email": ""
    }
]

```
- `POST /api/v2/estabelecimentos/{id}/clientes` [Cadastrar um cliente em um determinado estabelecimento]
```javascript
// Request
{
    "nome": "",
    "email": ""
}
// Response
{
	"id": ""
}

```
- `GET /api/v2/estabelecimentos/{id}/clientes/{id_cliente}` [Retornar um cliente específico do estabelecimento]
```javascript
// Response
{
	"id": "",
	"nome":"",
	"email":"",
}

```
- `PUT /api/v2/estabelecimentos/{id}/clientes/{id_cliente}` [Atualizar um cliente específico do estabelecimento]  
```javascript
// Request
{
	"nome":"",
	"email":""
}

// Response
{
	"id": "",
	"nome":"",
	"email":"",
}

```

Para retornar as informações dos endpoints, organize um simples banco de dados MySQL com uma tabela "estabelecimento" e relacione ela com uma tabela "cliente", e uma tabela "admin" que será usada
para autenticação.

Sugestão de tabelas e colunas:

admin [id, nome, email, senha, token]
cliente [id, nome, email]
estabelecimento [id, nome]
estabelecimento_cliente [estabelecimento_id, cliente_id]

Utilizar o eloquent ORM para estruturação dos Models  

Utilize um middleware para autenticar os requests realizados em todas as rotas exceto `POST api/v2/login`  
- Para autenticar o request, é obrigatório que o token gerado no login seja enviado no Header da requisição,
`Authorization: Bearer <TOKEN-AQUI>`  

Caso não seja enviado o token na requisição, retornar um JSON informando que o acesso foi negado e o código de status 401 Unauthorized 
  
Além dos itens acima, qualquer adicional ao projeto que você desejar colocar será muito bem vindo, e com certeza irá contar pontos no seu teste.  
  
  
  
### Envio do Teste ###
Suba o código e o SQL do database gerado em um repositório (Github ou Bitbucket) e envie o link para o email johnny@fidelizi.com.br


